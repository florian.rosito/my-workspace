"use strict";
const img = {
    tagName: 'img',
    class: '.demo',
    attributes: {
        alt: 'demo'
    }
};
const input = {
    tagName: 'input',
    attributes: {
        name: 'demo',
        type: 'text'
    }
};
const p = {
    tagName: 'p',
    attributes: {
        name: 'demo'
    }
};
