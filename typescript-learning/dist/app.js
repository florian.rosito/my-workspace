const compteur = document.querySelector('#compteur');
let i = 0;
const increment = (e) => {
    i++;
    const span = compteur.querySelector('span');
    if (span) {
        // Grace au type narrowing, on est sûre que span existe.
        span.innerText = i.toString();
    }
};
compteur === null || compteur === void 0 ? void 0 : compteur.addEventListener('click', increment);
export class Point {
    constructor() {
        this.x = 0;
        this.y = 0;
    }
    move(x, y) {
        this.x += x;
        this.y += y;
        return this;
    }
}
// Exemple d'utilisation de fichier de déclaration.
window.ga('send', {
    hitType: 'event',
    eventCategory: 'Utilisation du site',
});
// Import d'une librairie tier sans fichier de déclaration.
// On a créer un fichier de déclaration scroll-to.d.ts pour utiliser cette librairie.
import scrollTo from 'scroll-to';
scrollTo(500, 1200, {
    ease: 'out-bounce',
    duration: 1500
});
