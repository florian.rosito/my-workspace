type HTMLAttributes = {
    img: {
        alt?: string
    },
    input: {
        type?: 'text' | 'number',
        name?: string
    }
    p: {
        name?: string
    }
}

type HTMLNode<TagName extends keyof HTMLAttributes> = {
    tagName: TagName,
    class?: string,
    id?: string,
    attributes?: HTMLAttributes[TagName]
}

type ValueOF<T> = T[keyof T]

type HTMLShapes = {
    [key in keyof HTMLAttributes]: HTMLNode<key>
}

type HTMLShape = ValueOF<HTMLShapes>


const img: HTMLShape = {
    tagName: 'img',
    class: '.demo',
    attributes: {
        alt: 'demo'
    }
}

const input: HTMLShape = {
    tagName: 'input',
    attributes: {
        name: 'demo',
        type: 'text'
    }
}

const p: HTMLShape = {
    tagName: 'p',
    attributes:{
        name: 'demo'
    }
}