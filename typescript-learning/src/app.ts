const compteur = <HTMLButtonElement>document.querySelector<HTMLButtonElement>('#compteur');

let i = 0;

const increment = (e: Event): void => {
    i++;
    
    const span = compteur.querySelector('span');
    
    if (span) {
        // Grace au type narrowing, on est sûre que span existe.
        span.innerText = i.toString();
    }
}

compteur?.addEventListener('click', increment);

export class Point {
    x = 0;
    y = 0;

    move(x: number, y: number): this {
        this.x += x;
        this.y += y;

        return this;
    }
}

// Exemple d'utilisation de fichier de déclaration.
window.ga('send', {
    hitType: 'event',
    eventCategory: 'Utilisation du site',
})


// Import d'une librairie tier sans fichier de déclaration.
// On a créer un fichier de déclaration scroll-to.d.ts pour utiliser cette librairie.
import scrollTo from 'scroll-to';
 
scrollTo(500, 1200, {
  ease: 'out-bounce',
  duration: 1500
});