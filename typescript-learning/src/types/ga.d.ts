interface Window {
    ga: (eventName: string, option: {
        hitType: string,
        eventCategory?: string
    }) => void
}

/*
declare var ga: (eventName: string, option: {
    hitType: string,
    eventCategory?: string
}) => void
 */