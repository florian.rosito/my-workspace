import React, { Component } from 'react';

class App extends Component {
    render() {
        return (
            <div>
                Salut à tous de SSR {this.props.data}
            </div>
        );
    }
}

export default App;