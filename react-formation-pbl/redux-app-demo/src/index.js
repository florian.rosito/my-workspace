import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// Importation du module react-redux
import {Provider} from 'react-redux'

// Importation du fichier store
import store from './redux/store'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={store}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider> 
);
