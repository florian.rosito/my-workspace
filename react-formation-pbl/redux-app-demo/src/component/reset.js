import React from 'react'
import {connect} from 'react-redux'

import {resetCounter} from './../redux/Counter/counter.actions'

function Reset(props) {
    return (
        <button onClick={() => props.resetCounter()}>
            Remise à zéro
        </button>
    )
}

const mapStateToProps = state => {
    return {
        count: state.counter.count
    }
}

const mapDispatchToProps = dispatch => {
    return {
        resetCounter: () => dispatch(resetCounter()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Reset)