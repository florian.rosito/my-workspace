import React from 'react'

function Header(props) {
    const hello = "Hello App React from App.js !"

    return (
        <>
            <h1>{hello}</h1>
            <p style={{color: 'blue'}}>{props.desc}</p>
            {/* <p>Code commenté</p> */}
        </>
    )
}

export default Header