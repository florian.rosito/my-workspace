import React from 'react'
import ReactDOM from 'react-dom'

import App from './components/App'

ReactDOM.render(<App desc="Premiére démo de composant basique depuis Header" />, document.getElementById('root'))