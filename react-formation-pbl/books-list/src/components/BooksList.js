import React from "react";
import {Link} from "react-router-dom"

let BooksList = (props) => {
    return(
        <>
            <Link className="btn btn-success" to="/books/add">Ajouter un livre</Link>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col1">#</th>
                        <th scope="col1">Titre</th>
                        <th scope="col1">Auteur</th>
                        <th scope="col1">Annee</th>
                        <th scope="col1">Prix</th>
                        <th scope="col1">Editer</th>
                        <th scope="col1">Supprimer</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        props.books.map(
                            book => {
                                return (
                                    <tr key={book.id}>
                                        <td>{book.id}</td>
                                        <td>{book.titre}</td>
                                        <td>{book.auteur}</td>
                                        <td>{book.annee}</td>
                                        <td>{book.prix}</td>
                                        <td>
                                            <Link className="btn btn-primary" to={"/books/edit/"+book.id}>Editer</Link>
                                        </td>
                                        <td>
                                            <button className="btn btn-danger" onClick={() => props.deleteBooks(book.id)}>Supprimer</button>
                                        </td>
                                    </tr>
                                )
                            }
                        )
                    }
                </tbody>
            </table>
        </>
    )
}

export default BooksList