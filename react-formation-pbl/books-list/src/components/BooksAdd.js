import React, { useState } from "react";

const BooksAdd = (props) => {
    const [book, setBook] = useState ({
        id: Math.floor(Math.random() * 1000),
        titre: "",
        auteur: "",
        annee: 0,
        prix: 0,
    })
    
    const onFieldChanged = ({target}) => {
        setBook({...book, [target.name] : target.value})
    }

    return (
        <form onSubmit={(event)=>props.handleAddBooks(book, event)}>
            <div className="nb-3">
                <label htmlFor="book-titre" className="form-label">Titre</label>
                <input type="text" className="from-control" id="book-titre" value={book.titre} name="titre" onChange={(event) => onFieldChanged(event)} />
            </div>
            <div className="nb-3">
                <label htmlFor="book-auteur" className="form-label">Auteur</label>
                <input type="text" className="from-control" id="book-auteur" value={book.auteur} name="auteur" onChange={(event) => onFieldChanged(event)} />
            </div>
            <div className="nb-3">
                <label htmlFor="book-annee" className="form-label">Annee</label>
                <input type="number" className="from-control" id="book-annee" value={book.annee} name="annee" onChange={(event) => onFieldChanged(event)} />
            </div>
            <div className="nb-3">
                <label htmlFor="book-prix" className="form-label">Prix</label>
                <input type="number" className="from-control" id="book-prix" value={book.prix} name="prix" onChange={(event) => onFieldChanged(event)} />
            </div>
            <button type="submit" className="btn btn-primary">Ok</button>
        </form>
    )
}

export default BooksAdd