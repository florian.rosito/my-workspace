import React, { useState, useEffect } from "react";
import BooksList from "./BooksList";
import BooksAdd from "./BooksAdd";
import BooksEdit from "./BooksEdit";
import {Route, Routes, useNavigate} from 'react-router-dom'

const App = () => {
    const baseApiUrl = "http://localhost:3000/books"
    // Pour gérer l'état du composant dans une fonction, on doit passer par les hooks (useState)
    const [books, setBooks] = useState([])
    useEffect(
        () => {
            fetch(baseApiUrl)
                .then((response) => { return response.json() })
                .then((response) => setBooks([...response]))
        },
        []
    )

    const navidate = useNavigate()

    let deleteBooks = (idBook) => {
        if (window.confirm("êtes-vous sûre de vouloir supprimer le livre ?")) {
            const requestOptions = {
                method: 'DELETE'
            };
            
            fetch(baseApiUrl+'/'+idBook, requestOptions)
                .then((response) => response.json()) 
                .then(data => setBooks(books.filter(
                    book => book.id !== idBook
                )));
        }
    }

    let AddBooks = (book, event) => {
        event.preventDefault()

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(book)
        };

        fetch(baseApiUrl, requestOptions)
            .then((response) => response.json()) 
            .then(data => setBooks([...books, data]));    
            
        navidate("/books")
    }

    const editBook = (bookEdited, event) => {
        event.preventDefault()
        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(bookEdited)
        };

        fetch(baseApiUrl+'/'+bookEdited.id, requestOptions)
            .then((response) => response.json()) 
            .then(data => setBooks(books.map(
                book => {
                    if (book.id === data.id) {
                        return bookEdited
                    }
    
                    return book;
                }
            )));

        
        navidate("/books")
    }

    const getBookById = (id) => {
        return books.find(
            book => book.id === id
        )
    }

    return (
        <div className="container">
            <h1>Gestion des livres</h1>

            <Routes>
                <Route path="/books" exact element={<BooksList deleteBooks={deleteBooks} books={books} />} />
                <Route path="/books/add" element={<BooksAdd handleAddBooks={AddBooks} />} />
                <Route path="/books/edit/:id" element={<BooksEdit getBookById={getBookById} handleEditBook={editBook} />} />
            </Routes>
        
        {/*
            <div>
                <BooksList books={books} deleteBooks={deleteBooks} showEditBook={showEditBook} showAddBook={showAddBook} />
            </div>
            {action === "add" && (
                <div className="book-add-block">
                    <h2>Ajout d'un livre</h2>
                    <BooksAdd handleAddBooks={AddBooks} />
                </div>
            )}
    
            {action === "edit" && (
                <div className="book-add-block">
                    <h2>Edition d'un livre</h2>
                    <BooksEdit book={bookToEdit} handleEditBook={editBook}/>
                </div>
            )}
        */}
        </div>
    )
}

export default App