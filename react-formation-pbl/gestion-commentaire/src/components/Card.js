import React from 'react'

function Card(props) {
    if (props.commentStatus !== "declined") {
        return <div className="ui cards">
            <div className="card" style={{backgroundColor: (props.commentStatus === "approved" ? "green" : "white")}}>
                <div className="content">
                    {props.children}
                </div>
                <div className="extra content">
                <div className="ui two buttons">
                    <div className="ui basic green button" onClick={()=>props.handleStatusChange(props.id, "approved")}>Approve</div>
                    <div className="ui basic red button" onClick={()=>props.handleStatusChange(props.id, "declined")}>Decline</div>
                </div>
                </div>
            </div>
        </div>
    } else {
        return <></>
    }
}

export default Card