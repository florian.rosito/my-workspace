import React from 'react'
import Card from './Card'
import Comment from './Comment'

// Création d'un composant App avec une syntax de type fonction.
function AppFunction() {
    let comments = [
        {
            id: 1,
            photo: 'https://semantic-ui.com/images/avatar/small/stevie.jpg', 
            nom: 'Stevie Feliciano',
            text: 'Hey guys, I hope this example comment is helping you read this documentation.',
            score: '5 Faves',
            date: '2 days ago',
            commentStatus: 'pending'
        },
        {
            id: 2,
            photo: 'https://semantic-ui.com/images/avatar/small/joe.jpg', 
            nom: 'Bob Dylan',
            text: 'J\'ai mangé une pomme hier !',
            score: '1 Faves',
            date: '1 year ago',
            commentStatus: 'pending'
        },
        {
            id: 3,
            photo: 'https://semantic-ui.com/images/avatar/small/jenny.jpg', 
            nom: 'Rebecca Rabbit',
            text: 'I love carrots !',
            score: '3 Faves',
            date: '4 hours ago',
            commentStatus: 'pending'
        }
    ]

    let changeStatus = (id, newStatus) => {
        comments.map(
            comment => {
                if (comment.id == id ) {
                    comment.commentStatus = newStatus
                }

                return comment
            }        
        )
    }

    return (
        <>
            {
                comments.map(
                    comment => (
                    <Card key={comment.id} handleStatusChange = {changeStatus}>
                        <Comment key={comment.id}
                            photo={comment.photo}
                            nom={comment.nom}
                            text={comment.text}
                            score={comment.score}
                            date={comment.date} 
                        />
                    </Card>
                    )
                )
            }
            
            {/* 
                <Comment photo={comments[0].photo} nom={comments[0].nom} text={comments[0].text} score={comments[0].score} date={comments[0].date} />
                <Comment photo={comments[1].photo} nom={comments[1].nom} text={comments[1].text} score={comments[1].score} date={comments[1].date} />
                <Comment photo={comments[2].photo} nom={comments[2].nom} text={comments[2].text} score={comments[2].score} date={comments[2].date} />
            */}
        </>
    )
}

export default AppFunction