import React from 'react'

function Comment(props) {
    return (
        <div className="ui comments">
            <div className="comment">
                <a className="avatar">
                <img src={props.photo} />
                </a>
                <div className="content">
                <a className="author">{props.nom}</a>
                <div className="metadata">
                    <div className="date">{props.date}</div>
                    <div className="rating">
                    <i className="star icon"></i>
                       {props.score}
                    </div>
                </div>
                <div className="text">
                    {props.text}
                </div>
                </div>
            </div>
        </div>
    )
}

export default Comment