import React from 'react'
import Card from './Card'
import Comment from './Comment'

// Création d'un composant App avec une syntax de type objet.
class AppClass extends React.Component {

    /* Syntax compléte qui est normalement auto générer par React
    constructor(props) {
        super(props)
        this.state = {
            comments: [
                {
                    id: 1,
                    photo: 'https://semantic-ui.com/images/avatar/small/stevie.jpg', 
                    nom: 'Stevie Feliciano',
                    text: 'Hey guys, I hope this example comment is helping you read this documentation.',
                    score: '5 Faves',
                    date: '2 days ago',
                    commentStatus: 'pending'
                },
                {
                    id: 2,
                    photo: 'https://semantic-ui.com/images/avatar/small/joe.jpg', 
                    nom: 'Bob Dylan',
                    text: 'J\'ai mangé une pomme hier !',
                    score: '1 Faves',
                    date: '1 year ago',
                    commentStatus: 'pending'
                },
                {
                    id: 3,
                    photo: 'https://semantic-ui.com/images/avatar/small/jenny.jpg', 
                    nom: 'Rebecca Rabbit',
                    text: 'I love carrots !',
                    score: '3 Faves',
                    date: '4 hours ago',
                    commentStatus: 'pending'
                }
            ]
        }
    }
    */

    state = {
        comments: [
            {
                id: 1,
                photo: 'https://semantic-ui.com/images/avatar/small/stevie.jpg', 
                nom: 'Stevie Feliciano',
                text: 'Hey guys, I hope this example comment is helping you read this documentation.',
                score: '5 Faves',
                date: '2 days ago',
                commentStatus: 'pending'
            },
            {
                id: 2,
                photo: 'https://semantic-ui.com/images/avatar/small/joe.jpg', 
                nom: 'Bob Dylan',
                text: 'J\'ai mangé une pomme hier !',
                score: '1 Faves',
                date: '1 year ago',
                commentStatus: 'pending'
            },
            {
                id: 3,
                photo: 'https://semantic-ui.com/images/avatar/small/jenny.jpg', 
                nom: 'Rebecca Rabbit',
                text: 'I love carrots !',
                score: '3 Faves',
                date: '4 hours ago',
                commentStatus: 'pending'
            }
        ]
    }

    changeStatus = (id, newStatus) => {
        // On fait comme ça pour faire un clone de this.state.comments
        // car en JS faire un simple "=" fait un passage en référence (ce qui est un peu nul ...)
        let commentsCopy = this.state.comments.map(
            comment => {
                if (comment.id === id ) {
                    return ({...comment, commentStatus: newStatus}) 
                }

                return comment
            }        
        )
        this.setState({comments: commentsCopy})
    }

    render() {
        return (
            <>
                {
                    this.state.comments.map(
                        comment => (
                        <Card key={comment.id} commentStatus={comment.commentStatus} handleStatusChange = {this.changeStatus} id={comment.id}>
                            <Comment key={comment.id}
                                photo={comment.photo}
                                nom={comment.nom}
                                text={comment.text}
                                score={comment.score}
                                date={comment.date} 
                            />
                        </Card>
                        )
                    )
                }
                
                {/* 
                    <Comment photo={comments[0].photo} nom={comments[0].nom} text={comments[0].text} score={comments[0].score} date={comments[0].date} />
                    <Comment photo={comments[1].photo} nom={comments[1].nom} text={comments[1].text} score={comments[1].score} date={comments[1].date} />
                    <Comment photo={comments[2].photo} nom={comments[2].nom} text={comments[2].text} score={comments[2].score} date={comments[2].date} />
                */}
            </>
        )
    }    
}

export default AppClass