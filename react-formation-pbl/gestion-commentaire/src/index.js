import React from 'react'
import ReactDOM from 'react-dom'

import App from './components/AppClass'

ReactDOM.render(<App />, document.getElementById('root'))