import React, { useState } from "react";
import BooksList from "./BooksList";
import BooksAdd from "./BooksAdd";
import BooksEdit from "./BooksEdit";

const App = () => {
    // Pour gérer l'état du composant dans une fonction, on doit passer par les hooks (useState)
    const [books, setBooks] = useState([
        {
            id: 1,
            titre: "Calvin & Hobbes",
            auteur: "Bill Waterson",
            annee: 1989,
            prix: 20.0
        },
        {
            id: 2,
            titre: "One piece",
            auteur: "Eichiro Oda",
            annee: 2001,
            prix: 6.50
        },
        {
            id: 3,
            titre: "Titeuf",
            auteur: "Zep",
            annee: 2022,
            prix: 13.0
        }
    ])

    const [bookToEdit, setBookToEdit] = useState({})

    const [action, setAction] = useState("")

    let deleteBooks = (idBook) => {
        if (window.confirm("êtes-vous sûre de vouloir supprimer le livre ?")) {
            setBooks(books.filter(
                book => book.id !== idBook
            ))
        }
    }
    
    const showAddBook = () => {
        setAction("add")
    }

    let AddBooks = (book, event) => {
        event.preventDefault()
        setBooks([...books, book])
        setAction("")
    }

    const showEditBook = (book) => {
        setAction("edit")
        setBookToEdit(book)
    }

    const editBook = (bookEdited, event) => {
        event.preventDefault()
        setBooks(books.map(
            book => {
                if (book.id === bookEdited.id) {
                    return bookEdited
                }

                return book;
            }
        ))
        setAction("")
    }

    return (
        <>
            <div>
                <h1>Gestion des livres</h1>
            </div>
            <div>
                <BooksList books={books} deleteBooks={deleteBooks} showEditBook={showEditBook} showAddBook={showAddBook} />
            </div>
            {action === "add" && (
                <div className="book-add-block">
                    <h2>Ajout d'un livre</h2>
                    <BooksAdd handleAddBooks={AddBooks} />
                </div>
            )}
    
            {action === "edit" && (
                <div className="book-add-block">
                    <h2>Edition d'un livre</h2>
                    <BooksEdit book={bookToEdit} handleEditBook={editBook}/>
                </div>
            )}
            
        </>
    )
}

export default App