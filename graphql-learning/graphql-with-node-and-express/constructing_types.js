var express = require("express")
var { graphqlHTTP } = require("express-graphql")
var graphql = require("graphql")

var fakeDatabase = {
    jean: {
        id: "jean",
        name: "Jean Dupont",
    },
    alice: {
        id: "alice",
        name: "Alice Potou",
    },
}

// Define the User type
var userType = new graphql.GraphQLObjectType({
    name: "User",
    fields: {
        id: { type: graphql.GraphQLString },
        name: { type: graphql.GraphQLString },
    }
})

// Define the Query type
var queryType = new graphql.GraphQLObjectType({
    name: "Query",
    fields : {
        user: {
            type: userType,
            // `args` describes the arguments that the `user` query accepts
            args: {
                id: { type: graphql.GraphQLString },
            },
            resolve: (_, { id }) => {
                return fakeDatabase[id]
            },
        },
    },
})

var schema = new graphql.GraphQLSchema({ query: queryType })

var app = express()
app.use(
  "/graphql",
  graphqlHTTP({
    schema: schema,
    graphiql: true,
  })
)
app.listen(4000)
console.log("Running a GraphQL API server at localhost:4000/graphql")

/*

Exemple de requête à passer : 

{
  user(id: "alice") {
    name
  }
}


*/