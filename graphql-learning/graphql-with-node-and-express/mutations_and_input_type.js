var express = require("express")
var { graphqlHTTP } = require("express-graphql")
var { buildSchema } = require("graphql")

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
    input MessageInput {
        content: String
        author: String
    }

    type Message {
        id: ID!
        content: String
        author: String
    }

    type Mutation {
        createMessage(input: MessageInput): Message
        updateMessage(id: ID!, input: MessageInput): Message
    }

    type Query {
        getMessage(id: ID!): Message
    }
`)

// If Message had any complex fields, we'd put them on this objet.
class Message {
    constructor(id, {content, author}) {
        this.id = id,
        this.content = content,
        this.author = author
    }
}

// Maps username to content
var fakeDatabase = {}

// The root provides a resolver function for each API endpoint
var root = {
    getMessage: ({ id }) => {
        if (!fakeDatabase[id]) {
            throw new Error("no message exists with id " + id)
        }

        return new Message(id, fakeDatabase[id])
    },
    createMessage: ({ input }) => {
        // Create a random id for our "database"
        var id = require("crypto").randomBytes(10).toString("hex")

        fakeDatabase[id] = input
        return new Message(id, input)
    },
    updateMessage: ({ id, input }) => {
        if (!fakeDatabase[id]) {
            throw new Error("no message exists with id " + id)
        }
        // This replaces all old data, but some apps might want partial update.
        fakeDatabase[id] = input
        return new Message(id, input)
    }
}

var app = express()
app.use(
  "/graphql",
  graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
  })
)
app.listen(4000)
console.log("Running a GraphQL API server at localhost:4000/graphql")

/*

Exemple de requête à passer : 

Pour la création : 

mutation {
  createMessage(input: {
    author: "Jean Dupont",
    content: "Salut comment ça va ?"
  }) {
    id
  }
}

Pour récupérer un message :

{
  getMessage(id: "a32854250eec20ca4e61") {
    id,
    content,
    author
  }
}

Pour "mettre à jour"  un message :

mutation {
  updateMessage(
    id: "a32854250eec20ca4e61",
    input: {
      author: "Marie Patou",
      content: "ça va merci :)"
    }) {
      id
    }
}

*/