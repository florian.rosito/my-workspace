import { ApolloServer } from "apollo-server";
// Plugin pour avoir un playground offline pour tester son schema.
import { ApolloServerPluginLandingPageGraphQLPlayground } from "apollo-server-core";

// L'objet schema que l'on injecte au serveur Apollo afin qu'il connaisse ce dernier.
import { schema } from "./schema";
import { context } from "./context";

export const server = new ApolloServer({
    schema,
    context,
    plugins: [ApolloServerPluginLandingPageGraphQLPlayground()],
});

const port = 3000;
// Démarrage du serveur sur le port 3000. L'url sera retourner dans une promise.
server.listen({port}).then(({ url }) => {
    console.log(`🚀  Server ready at ${url}`);
});

/**
 * Requête à faire :
 * 
 * query Query {
 *  ok
 * }
 * 
 * Réponse :
 * 
 * {
 *  "data": {
 *      "ok": true
 *  }
 * }
 * 
 */