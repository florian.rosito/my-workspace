import { extendType, intArg, nonNull, objectType, stringArg } from "nexus";
import { NexusGenObjects } from "../../nexus-typegen";

export const Link = objectType({
    name: "Link",
    definition(t) {
        t.nonNull.int("id");
        t.nonNull.string("description");
        t.nonNull.string("url");
        t.field("postedBy", {
            type: "User",
            resolve(parent, args, context) {
                return context.prisma.link
                    .findUnique({ where: { id: parent.id } })
                    .postedBy();
            },
        });
    },
});

export const LinkQuery = extendType({
    type: "Query",
    definition(t) {
        t.nonNull.list.nonNull.field("feed", {
           type: "Link",
           resolve(parent, args, context, info) {
                return context.prisma.link.findMany();
           }, 
        });
        
        t.nullable.field("link", {
           type: "Link",
           args: {
               id: nonNull(intArg()),
           },
           resolve(parent, args, context, info) {
                return context.prisma.link.findUnique({
                    where: {
                        id: args.id
                    }
                });
           }, 
        });
    },
});

/**
 * Exemples de requêtes
 * 
 *   query {
 *     feed {
 *          id
 *          description
 *          url
 *       }
 *   }
 */ 

export const LinkMutation = extendType({
    type: "Mutation",
    definition(t) {
        t.nonNull.field("post", {
            type: "Link",
            args: {
                description: nonNull(stringArg()),
                url: nonNull(stringArg()),
            },
            resolve(parent, args, context) {
                const { description, url } = args;
                const { userId } = context;

                if (!userId) {
                    throw new Error("Cannot post without logging in.");
                }

                return context.prisma.link.create({
                    data: {
                        description: description,
                        url: url,
                        postedBy: { connect: { id: userId } },
                    },
                });
            },
        });

        t.nonNull.field("delete", {
           type: "Link",
           args: {
                id: nonNull(intArg()),
           },
           resolve(parent, args, context) {
                return context.prisma.link.delete({
                    where: {
                        id: args.id
                    }
                });
           } 
        });

        t.nonNull.field("update", {
            type: "Link",
            args: {
                id: nonNull(intArg()),
                description: nonNull(stringArg()),
                url: nonNull(stringArg()),
            },
            resolve(parent, args, context) {
                return context.prisma.link.update({
                    where: {
                        id: args.id
                    },
                    data : {
                        description: args.description,
                        url: args.url,
                    },
                });
           } 
        });
    },
});

/**
 * 
 * Exemple de requêtes
 * 
 * mutation {
 *    post(url: "www.prisma.io", description: "Next-generation Node.js and TypeScript ORM") {
 *      id
 *    }
 *  }
 */