import { makeSchema } from "nexus";
import { join } from 'path';
import * as types from "./graphql";

export const schema = makeSchema({
    // Liste d'objet que contiendera le schema.
    types,
    outputs: {
        schema: join(process.cwd(), "schema.graphql"), // Nexus générarera automatique un fichier contenant le schema au format SDL.
        typegen: join(process.cwd(), "nexus-typegen.ts"), // Fichier Typescript qui contiendera la liste des types présents dans notre schema.
     },
     contextType: {
        module: join(process.cwd(), "./src/context.ts"),
        export: "Context",
     }
})