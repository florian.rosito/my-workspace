<?php

namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\Model\Operation;
use ApiPlatform\Core\OpenApi\Model\PathItem;
use ApiPlatform\Core\OpenApi\Model\RequestBody;
use ApiPlatform\Core\OpenApi\OpenApi;
use ArrayObject;

class OpenApiFactory implements OpenApiFactoryInterface
{

    public function __construct(private OpenApiFactoryInterface $decorated) {}

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = $this->decorated->__invoke($context);

        // Suppression des paths contenant le "summary" "hidden".
        foreach($openApi->getPaths()->getPaths() as $key => $path) {
            if (!empty($path->getGet()) && $path->getGet()->getSummary() === 'hidden') {
                $openApi->getPaths()->addPath($key, $path->withGet(null));
            }
        }

        // Méthode pour ajouter un pathItem custom.
        $item = new PathItem(null, 'Ping', null, new Operation('ping-id', [], [], 'Pong !'));
        $openApi->getPaths()->addPath('/ping', $item);

        
        // Bloc permetant la mise en place d'un endpoint d'authentification via cookie dans OpenAPI.
        $securitySchemas = $openApi->getComponents()->getSecuritySchemes();
        // Authentification via Bearer (JWT)
        $securitySchemas['bearerAuth'] = new \ArrayObject([
            'type' => 'http',
            'scheme' => 'bearer',
            'bearerFormat' => 'JWT'
        ]);

        // Authentification via Cookie
        // $securitySchemas['cookieAuth'] = new \ArrayObject([
        //     'type' => 'apiKey',
        //     'in' => 'cookie',
        //     'name' => 'PHPSESSID'
        // ]);

        $schemas = $openApi->getComponents()->getSchemas();
        $schemas['Credentials'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'username' => [
                    'type' => 'string',
                    'example' => 'jean_test@gmail.com',
                ],
                'password' => [
                    'type' => 'string',
                    'example' => '0000',
                ]
            ]
        ]);

        $meOperation = $openApi->getPaths()->getPath('/api/me')->getGet()->withParameters([]);
        $mePathItem = $openApi->getPaths()->getPath('/api/me')->withGet($meOperation);
        $openApi->getPaths()->addPath('/api/me', $mePathItem);

        $pathItem = new PathItem(
            post: new Operation(
                operationId: 'postApiLogin',
                tags: ['User'],
                requestBody: new RequestBody(
                    content: new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' =>'#/components/schemas/Credentials'
                            ]
                        ]
                    ])
                ),
                responses: [
                    '200' => [
                        'description' => 'Utilisateur connecté',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/User-read.user'
                                ]
                            ]
                        ]
                    ]
                ]
            )
        );
        $openApi->getPaths()->addPath('/api/login', $pathItem);

        $pathItem = new PathItem(
            post: new Operation(
                operationId: 'postApiLogout',
                tags: ['User'],
                responses: [
                    '204' => []
                ]
            )
        );
        $openApi->getPaths()->addPath('/logout', $pathItem);

        return $openApi;
    }
}