<?php

namespace App\Controller;

use App\Repository\PostRepository;
use Symfony\Component\HttpFoundation\Request;

class PostCountController
{
    public function __invoke(Request $request, PostRepository $postRepository): int 
    {
        $criteria = (boolean) $request->query->get('online') === true ? ['online' => true] : [];
        
        return $postRepository->count($criteria);
    }
}