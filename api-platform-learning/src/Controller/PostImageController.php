<?php

namespace App\Controller;

use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class PostImageController extends AbstractController
{
    public function __invoke(Request $request)
    {
        $post = $request->attributes->get('data');
        if (!($post instanceof Post)) {
            throw new \RuntimeException('Post required !');
        }

        $post->setFile($request->files->get('file'));
        // Il faut obligatoirement faire un changement de champs présent en BDD pour que Vich éxécute
        // l'upload et l'enregistrement de l'info en base car ce dernier se base sur des events Doctrine.
        $post->setUpdatedAt(new \DateTimeImmutable());

        return $post;
    }
}