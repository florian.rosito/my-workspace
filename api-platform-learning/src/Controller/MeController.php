<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Security;

class MeController
{
    public function __invoke(Security $security)
    {
        $user = $security->getUser();

        return $user;
    }
}