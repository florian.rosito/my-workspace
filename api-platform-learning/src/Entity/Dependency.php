<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

#[ApiResource(
    paginationEnabled: false,
    itemOperations: [
        'get',
        'delete',
        'put' => [
            'denormalization_context' => [
                'groups' => ['put:dependency']
            ]
        ]
    ],
    collectionOperations: ['get', 'post']
)]
class Dependency
{
    #[ApiProperty(
        identifier: true
    )]
    private string $uuid;

    #[
        ApiProperty(
            description: 'Nom de la dépendance'
        ),
        Length(min: 2),
        NotBlank()
    ]
    private string $name;

    #[
        ApiProperty(
            description: 'Version de la dépendance',
            example: '5.4.*'
        ),
        NotBlank(),
        Groups(['put:dependency'])
    ]
    private string $version;

    public function __construct(
        string $name,
        string $version
    )
    {
        $this->uuid = Uuid::uuid5(Uuid::NAMESPACE_URL, $name)->__toString();
        $this->name = $name;
        $this->version = $version;
    }

    // On ajoute uniquement les getters parce que les setters sont gérer lors de la construction de l'objet.

    public function getUuid(): string 
    {
        return $this->uuid;
    }

    public function getName(): string 
    {
        return $this->name;
    }

    public function getVersion(): string 
    {
        return $this->version;
    }

    public function setVersion(string $version): self 
    {
        $this->version = $version;

        return $this;
    }
}