<?php

namespace App\Serializer;

use ApiPlatform\Core\Exception\RuntimeException;
use ApiPlatform\Core\Serializer\SerializerContextBuilderInterface;
use App\Entity\Post;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Security;

class PostContextBuilder implements SerializerContextBuilderInterface
{
    public function __construct(
        private SerializerContextBuilderInterface $decorated,
        private Security $security
    ) {}

    public function createFromRequest(Request $request, bool $normalization, array $extractedAttributes = null): array
    {
        $context = $this->decorated->createFromRequest($request, $normalization, $extractedAttributes);

        $resourceClass = $context['resource_class'] ?? null;

        if (
            $resourceClass === Post::class &&
            isset($context['groups']) &&
            !empty($this->security->getUser())
        ) {
            if (is_array($context['groups'])) {
                $context['groups'][] = 'read:collection:User';
            } else {
                $tmp = $context['groups'];
                $context['groups'] = [
                    $tmp,
                    'read:collection:User'
                ];
            }
        }
        return $context;
    }
}