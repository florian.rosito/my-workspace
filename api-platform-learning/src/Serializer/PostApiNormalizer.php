<?php

namespace App\Serializer;

use App\Entity\Post;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Exception\CircularReferenceException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\LogicException;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;

class PostApiNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    private const ALREADY_CALLED_NORMALIZER = 'AlreadyCalled';

    public function __construct(private Security $security) {}

    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        $alreadyCalled = $context[self::ALREADY_CALLED_NORMALIZER] ?? false;

        return $data instanceof Post && $alreadyCalled === false;
    }

    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        $context[self::ALREADY_CALLED_NORMALIZER] = true;
        if (
            !empty($this->security->getUser()) &&
            isset($context['groups']) &&
            !empty($object->getUser()) &&
            $this->security->getUser()->getId() === $object->getUser()->getId()
        ) {
            if (is_array($context['groups'])) {
                $context['groups'][] = 'read:collection:User';
            } else {
                $tmp = $context['groups'];
                $context['groups'] = [
                    $tmp,
                    'read:collection:User'
                ];
            }
        }

        return $this->normalizer->normalize($object, $format, $context);
    }
}