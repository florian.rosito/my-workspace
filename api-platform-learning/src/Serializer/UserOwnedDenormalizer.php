<?php

namespace App\Serializer;

use App\Entity\Post;
use App\Entity\UserOwnedInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class UserOwnedDenormalizer implements ContextAwareDenormalizerInterface, DenormalizerAwareInterface
{
    // Permet de faire appel aux autres denormalizer de l'application.
    use DenormalizerAwareTrait;

    private const ALREADY_CALLED_DENORMALIZER = 'UserOwnedDenormalizerCalled';

    public function __construct(private Security $security) {}

    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return false;
        $reflectionClass = new \ReflectionClass($type);
        $alreadyCalled = $context[$this->getAlreadyCalledKey($type)] ?? false;

        return $reflectionClass->implementsInterface(UserOwnedInterface::class) && $alreadyCalled === false;
    }

    public function denormalize(mixed $data, string $type, string $format = null, array $context = [])
    {
        $context[$this->getAlreadyCalledKey($type)] = true;
        /** @var Post $object */
        $object = $this->denormalizer->denormalize($data, $type, $format, $context);
        $object->setUser($this->security->getUser());

        return $object;
    }

    private function getAlreadyCalledKey(string $type)
    {
            return self::ALREADY_CALLED_DENORMALIZER.$type;
    }
}