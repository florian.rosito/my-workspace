// Création de composant de maniére fonctionnelle
// Utilisation de la syntaxe Welcome({name, children})
// équivaut à faire Welcome(props) { ... props.name ... props.children}
function WelcomeFunc({name, children}) {
    return <div> 
        <h1>Bonjour {name}</h1>
        <p>
            {children}
        </p>
    </div>
}

// Création de composant via une classe.
class Welcome extends React.Component {

    // Les propriétés sont toujours appelé au niveau du constructeur
    // IMPORTANT : il faut toujours initier le parent via la méthode super()
    constructor(props) {
        super(props)
    }

    render () {
        return <div> 
            <h1>Coucou {this.props.name}</h1>
            <p>
                {this.props.children}
            </p>
        </div>
    }
}

class Clock extends React.Component {

    constructor(props) {
        super(props)
        this.state = {date: new Date()}
        this.timer = null
    }

    componentDidMount() {
        this.timer = window.setInterval(this.tick.bind(this), 1000)
    }

    componentwillUnmount() {
        window.clearInterval(this.timer)
    }

    tick() {
        this.setState({date: new Date()})
    }

    render() {
        return <div>
            Il est {this.state.date.toLocaleDateString()} {this.state.date.toLocaleTimeString()}
        </div>
    }
}

class Incrementer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            i: props.start
        }
        this.timer = null
    }

    componentDidMount() {
        this.timer = window.setInterval(this.increment.bind(this), 1000)
    }

    componentwillUnmount() {
        window.clearInterval(this.timer)
    }

    increment() {
        // Si le changement d'état dépend d'un état ou une propriété, il fortement conseillé de passer par une fonction.
        this.setState(function (state, props) {
            return {i: state.i + props.step}
        })
    }

    render() {
        return <div>
            {this.state.i}
        </div>
    }
}

// Permet de définir des valeurs par défaut aux propriétés.
Incrementer.defaultProps = {
    start: 0,
    step: 1
}

function Home() {
    return <div>
        <Welcome name="Marc" children="Juju" ></Welcome>
        <Welcome name="Julie"></Welcome>
        <Welcome name="Robert"></Welcome>
        <Clock />
        <Incrementer start={43} />
        <Incrementer start={2} step={10}/>
        <Incrementer step={20}/>
    </div>
}

ReactDOM.render(<Home />, document.querySelector('#app'))