let n = 0

function numberFormat(n) {
   return n.toString().padStart(2, '0')
}

function render () {

    const items = [
        'Tache 1',
        'Tache 2',
        'Tache 3'
    ]

    const lis = items.map((item, k) => <li key={k}>{item}</li>)

    // En JXS, tout ce qui se trouve entre {} est du Javascript valide.
    // class étant un mot réserver par React, en JXS il faut passer par l'attribut className pour définir une classe HTML
    const title = <React.Fragment>
        <h1 className="title" id="title">
        Salut les gens :) <span>{ numberFormat(n) }</span>
        </h1>
        <ul>
            {lis}
        </ul>
        </React.Fragment>

    ReactDOM.render(title, document.querySelector('#app'))
}

render()

window.setInterval(() => {
    n++
    render()
}, 1000)

// Exemple de code sans JSX.

// let n = 0

// function render () {
//     const title = React.createElement('h1', {}, 'Bonjour à tous :) ', React.createElement('span', {}, n))

//     ReactDOM.render(title, document.querySelector('#app'))
// }

// render()

// window.setInterval(() => {
//     n++
//     render()
// }, 1000)