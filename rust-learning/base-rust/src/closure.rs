pub fn closure() {
    println!("Closure");

    calcul(addition, 25, 35);
    calcul(soustraction, 25, 35);

    // Il n'y a pas de fonction anonyme dans Rust, on passe par les closures pour ça.
    let a = 45;
    let mut b = 78;
    calcul(|x,y| x*y, a, b);
    // Une closure peut intéragir avec son environnement.
    calcul_env(|| a+b);
    // Création d'un scope
    {
        // Cela fonctionne toujours car la closure peut récupérer les éléments du scope parent
        calcul_env(|| a-b);
        let c = 20;
    }
    // Cela ne marche pas car le scope est détruit à ce moment là.
    // calcul_env(|| a+c);

    // Gestion de l'Ownership : si on utilise une closure avec un argument, le ownership s'appliquera
    // Mais si on passe directement par l'environnement, alors le borrowing sera appliqué automatiquement.
    // Le mot clé "move" permet de forcer le ownership.
    let str = String::from("Coucou");
    // let clos = move|| println!("{}", str);
    let clos = || println!("{}", str);
    clos();
    clos();
    println!("{}", str);
}

fn addition(x: i32, y: i32) -> i32 {
    x+y
}

fn soustraction(x: i32, y: i32) -> i32 {
    x-y
}

fn calcul<F>(function: F, a: i32, b: i32)
    where F: Fn(i32, i32) -> i32
{
    println!("{}", function(a, b));
}

fn calcul_env<F>(function: F)
    where F: Fn() -> i32
{
    println!("{}", function());
}