pub fn pointer() {
    println!("Références et Pointeurs");

    let mut a = String::from("TEST TEST");
    {
        // On peut faire un passage en référence de a vers b afin d'éviter de cloner a.
        // On appelle cela le "borrowing".
        let b = &mut a;

        // Cela ne passe pas car on ne peut avoir qu'un seul pointeur mutatble sur une variable.
        // let c = &mut a;

        b.push_str(" ou pas !");
        println!("{}", b);
    }
    // Comme le borrowing est liée au scope, on peut refaire un pointeur vers "a", un fois sortie que b est détruit.
    let d = &a;

    // Par contre attention, quand on passe par une référence, on modifie la valeur pointée.
    // Donc, "a" aura la nouvelle valeur assignée à "b".
    println!("{}", a);
}