// Equivalent des interfaces dans d'autres languages.
trait Surface {
    fn surface(&self) -> f32;
}

trait Perimetre {
    fn perimetre(&self) -> f32;
}

struct Rectangle {
    long: f32,
    larg: f32,
}

impl Surface for Rectangle {
    fn surface(&self) -> f32 {
        &self.larg * &self.larg
    }
}

impl Perimetre for Rectangle {
    fn perimetre(&self) -> f32 {
        (&self.larg + &self.larg) * 2.0f32
    }
}

struct Cercle {
    rayon: f32,
}

impl Surface for Cercle {
    fn surface(&self) -> f32 {
        &self.rayon.powf(2.0) * 3.14f32
    }
}

impl Perimetre for Cercle {
    fn perimetre(&self) -> f32 {
        &self.rayon * 2.0f32 * 3.14f32
    }
}

pub fn trait_rust() {
    println!("Traits");

    let r1 = Rectangle{long: 14.6, larg: 78.5};
    let c1 = Cercle{rayon: 20.3};
    calculer(&r1);
    calculer(&c1);
    calculer_diff(r1, c1);
}

fn calculer<T: Surface + Perimetre>(figure: &T) {
    println!("{}", figure.surface());
    println!("{}", figure.perimetre());
}

fn calculer_diff<T, U>(fig1: T, fig2: U)
    where T: Surface, U: Perimetre
{
    println!("{}", fig1.surface());
    println!("{}", fig2.perimetre());
}
