pub fn cond_stuct() {
    println!("Structures Conditionnelles");

    let a = 18;

    if a >= 18 {
        println!("majeur");
    } else if a > 0 {
        println!("mineur");
    } else {
        println!("erreur");
    }

    let b = if a >= 18 {
        "majeur"
    } else {
        "mineur"
    };
    println!("{}", b);
}