pub fn functions() {
    println!("Fonctions");

    println!("{}", addition(10, 45));

    let string = String::from("C'est un texte à la noix");
    say(&string);
    say(&string);
    // Cela ne marche pas, car les arguments sont passé en copie vers la fonction cible.
    // On se retrouve donc avec un probléme d'ownership.
    //      say(string)
    // Une solution pour éviter cela est de retourner la valeur dans la méthode "say".
    //      let string2 = say(string);
    //      say(string2);

    // L'autre méthode est simplement de passer une référence en paramétre de la méthode.
    // Cela s'appelle le "borrowing".

    // Autre exemple de l'utilité des références.
    let mut a = 5;
    modif(&mut a);
    println!("{}", a);

    let mut b = "Mince".to_owned();
    modifString(&mut b);
    println!("{}", b);
}

// On ne peut pas surcharger une fonction, à la différence des méthodes.
fn addition(x:i32, y:i32) -> i32 {
    // Equivalant à "return x + y;"
    x + y
}

fn say(txt: &String) {
    println!("{}", txt);
    // txt
}

fn modif(arg: &mut i32) {
    // * permet de faire un déréférencement une variable en référence afin de récupérer la valeur associée.
    *arg *= 5;
}

fn modifString(arg: &mut String) {
    // String étant déjà un pointeur, il n'y a pas besoin de faire de déréférencement.
    arg.push_str(" à tous !");
}

// Ceci n'est pas possible, car lorsque l'on sort d'une fonction, son contenu est détruit
// et donc le pointeur "a" n'existe plus.
// On verra plutard comment gérer cela avec la mécanique de lifetime.
// fn retour() -> &u8 {
//     let a:  u8 = 10;
//     &a
// }