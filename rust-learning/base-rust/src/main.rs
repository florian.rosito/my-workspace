use std::str::FromStr;
mod structure_conditionnelle;
mod pattern_matching;
mod pointers;
mod collections;
mod loops;
mod functions;
mod modules;
mod struct_rust;
mod enums_rust;
mod error_management;
mod genericity;
mod trait_rust;
mod lifetime;
mod closure;
mod concurrency;
mod input_output;

fn main() {
    // u8 u16 u32 u128 : entier non signé.
    // i8 i16 i32 i128 : entier signé.
    // usize isize : le size est définit par la taille du systéme. 
    // f32 f64 : float.
    // Notation binaire : let ma_variable = 0b10011;
    // Notation hexa : let ma_variable = 0xff;
    // Notation chiffre long: let ma_variable = 15_000_000;
    let ma_variable:u8 = 23;
    println!("Ma variable est égale à {}",ma_variable);

    let mut a: u16 = 23;
    a = 48;
    println!("{}", a);

    let b:u16 = 85u8 as u16;
    const MA_CONSTANTE:f32 = 14.52;
    println!("{} - {}", b, MA_CONSTANTE);

    let c:u16 = 45+47;
    println!("{}", c);

    let d:f32 = 85f32+12.5; // Pensez à cast quand on veut faire un int + float.
    println!("{}", d);

    let f:bool = true;
    println!("{}", f);
    let g:char = 'k';
    println!("{}", g);

    // Il existe deux types différents pour gérer les string
    // Différence : 1) le type String est mutable de base alors que le &str est immutable.
    // 2) le type String est un objet. Donc il embarque des méthodes qui permet des traitement plus élaborer sur la chaîne.
    let h: &str = "Ma chaîne de caractères";
    let mut i:String = String::from("Une autre chaîne");
    i.push_str(" plus longue que prévu");
    println!("{} \n{}", h, i);

    // Exemple de conversion sans shadowing
    let x: u16 = 256;
    let y = x.to_string();
    println!("{}", y);
    
    // Exemple de conversion avec shadowing
    let z: u16 = 256;
    let z = z.to_string();
    println!("{}", z);

    let test:u32 = 89;
    let mut foo = test;
    foo = 12;
    println!("{} - {}", test, foo);

    // Ce code génére une erreur car lorsque l'on copie une variable de stype struct
    // alors la copie du pointeur liée va pointé vers la même valeur.
    // Or, en Rust, une valeur ne peut assigné qu'a un pointeur.
    // D'où l'erreur : "value moved here"

        // let test2 = "Hello".to_owned();
        // let bar = test2;
        // println!("{} - {}", test2, bar);
        
    // Correction via la méthode clone
    let test2 = "Hello".to_owned();
    let mut bar = test2.clone();
    bar.push_str(" World");
    println!("{} - {}", test2, bar);

    // Ownership et scope
    let aa = "Test".to_owned();
    {
        let bb = aa;
        println!("{}", bb);
    }
    // Impossible à afficher car bb est définit dans un autre scope.
    // println!("{}", bb);
    println!("-----------------------------");
    println!("-----------------------------");
    structure_conditionnelle::cond_stuct();
    println!("-----------------------------");
    println!("-----------------------------");
    pattern_matching::pattern_matching();
    println!("-----------------------------");
    println!("-----------------------------");
    pointers::pointer();
    println!("-----------------------------");
    println!("-----------------------------");
    collections::collection();
    println!("-----------------------------");
    println!("-----------------------------");
    loops::loops();
    println!("-----------------------------");
    println!("-----------------------------");
    functions::functions();
    println!("-----------------------------");
    println!("-----------------------------");
    modules::modules();
    println!("-----------------------------");
    println!("-----------------------------");
    struct_rust::struct_rust();
    println!("-----------------------------");
    println!("-----------------------------");
    enums_rust::enums_rust();
    println!("-----------------------------");
    println!("-----------------------------");
    error_management::error_management();
    println!("-----------------------------");
    println!("-----------------------------");
    genericity::genericity();
    println!("-----------------------------");
    println!("-----------------------------");
    trait_rust::trait_rust();
    println!("-----------------------------");
    println!("-----------------------------");
    lifetime::lifetime();
    println!("-----------------------------");
    println!("-----------------------------");
    closure::closure();
    println!("-----------------------------");
    println!("-----------------------------");
    concurrency::concurrency();
    println!("-----------------------------");
    println!("-----------------------------");
    input_output::input_output();
}


