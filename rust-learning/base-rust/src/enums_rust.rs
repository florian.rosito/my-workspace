enum Choix {
    Pour,
    Contre(String),
}

// La définition d'une Option est un type fourni par Rust.
// Ce systéme permet de palier au fait qu'en Rust il n'y a pas de type null.
// enum Option<T> {
//     Some(T),
//     None,
// }

pub fn enums_rust() {
    println!("Enum");
    choisir(Choix::Contre("1 + 1 = 2".to_owned()));

    println!("Option");
    match op(14) {
        Option::None => println!("Aucune valeur"),
        Option::Some(x) => println!("Valeur : {}", x)
    }
}

fn op(arg: u8) -> Option<u8> {
    return Option::Some(arg);
}

// Les enums permettent de vraiment ciblé les options que l'on souhaite gérer.
fn choisir(choix: Choix) {
    match choix {
        Choix::Pour => println!("Plutôt Pour"),
        Choix::Contre(x) => println!("Plutôt Contre parce que {}", x)
    }
}