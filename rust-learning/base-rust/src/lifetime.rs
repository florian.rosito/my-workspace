pub fn lifetime() {
    println!("Lifetime");

    let a = 8;
    let b = 32;

    println!("{}", plus_grand(&a, &b));

    let c = Truc_A_Dire{texte: "Toto à la mer"};
    println!("{}", c.texte);
}

struct Truc_A_Dire<'a> {
    texte: &'a str,
}

// 'a permet de définir une durée du vie aux pointeurs et ainsi de syncro cette derniére avec la variable de retour.
fn plus_grand<'a>(var1: &'a u8, var2: &'a u8) -> &'a u8 {
    if var1 > var2 {
        return var1;
    }

    return var2;
}