use std::thread;
use std::time::Duration;
use std::sync::{mpsc, Mutex, Arc};

pub fn concurrency() {
    println!("Concurrency - Thread");

    // Ce thread sera arréter avant la fin de la boucle car il est inclue
    // dans le thread principal qui stop le programme dans son ensemble.
    let th = thread::spawn(|| {
        for i in 1..10 {
            println!("--------------------- Thread secondaire - {}", i);
            thread::sleep(Duration::from_millis(100));
        }
    });

    for i in 1..5 {
        println!("Thread principal - {}", i);
        thread::sleep(Duration::from_millis(60));
    }

    // Methode qui permet de liée un thread au thread principal afin que le thread principal
    // attende la fin du second thread avant de se terminer.
    th.join();

    channelExample();
    mutexExample();
}

fn channelExample() {
    println!("Concurrency - Channel");

    // Définition d'un channel, qui se constitue d'un sender et d'un receiver.
    let (sender, receiver) = mpsc::channel();

    // On peut dupliquer une sender de cette maniére.
    let sen2 = mpsc::Sender::clone(&sender);

    // Dans un premier thread, on écrit dans un sender.
    thread::spawn(move || {
        for i in 1..10 {
            sender.send(i).unwrap();
            thread::sleep(Duration::from_millis(500));
        }
    });

    // Dans un second, on écrit dans le deuxiéme sender.
    thread::spawn(move || {
        for i in 100..110 {
            sen2.send(i).unwrap();
            thread::sleep(Duration::from_millis(200));
        }
    });

    // Et on peut récupérer les écritures des deux sender dans le receiver.
    for message in receiver {
        println!("{}", message);
    }

    println!("Fin de progrmme");
}

fn mutexExample() {
    // Méthode permettant d'accéder à une même valeur depuis plusieurs thread.
    println!("Concurrency - Mutex");

    let mut count = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for _ in 0..1000 {
        let c = Arc::clone(&count);
        let handler = thread::spawn(move ||{
            // La méthode lock permet de bloquer
            // les autres thread le temps que le thread en cours finise.
            let mut num = c.lock().unwrap();
            *num += 1;
        });
        handles.push(handler);
    }

    for handle in handles {
        handle.join();
    }

    println!("{}", *count.lock().unwrap());
}