mod mon_module;

// Méthode pour importer un crate (dépendances extérieur).
extern crate rand;
// Ceci permet de charger toutes les fonctions d'un ensemble
// pour le script actuel.
// Ensemble prelude correspond aux fonctiones les plus utilisé de la dépendance.
use rand::prelude::*;

pub fn modules() {
    println!("Modularité");

    mon_module::hello();

    println!("{}", random::<i32>());
}