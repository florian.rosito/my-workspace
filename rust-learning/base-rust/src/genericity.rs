// Afin d'éviter de devoir créer des structs Rectangle pour les types numériques existants,
// Rust permet de créer des types génériques afin d'englober plusieurs types.
// Par convention les types génériques sont nommés avec une lettre en majuscules.
struct Rectangle<T> {
    longueur: T,
    largeur: T,
}

// Utilisation d'un type générique pour une méthode.
impl<T> Rectangle<T> {
    fn get_longueur(&self) -> &T {
        &self.longueur
    }
}

// Quand on utilise un type génériques, on est obligé de surcharger les méthodes (comme les opérateurs)
// Pour chaque types que l'on souhaite exploité.
impl Rectangle<usize> {
    fn surface(&self) -> usize {
        &self.longueur * &self.largeur
    }
}

impl Rectangle<f32> {
    fn surface(&self) -> f32 {
        &self.longueur * &self.largeur
    }
}

pub fn genericity() {
    println!("Généricité");

    let rec1 = Rectangle{longueur:15, largeur:23};
    let rec2 = Rectangle{longueur:15.5, largeur:23.25};

    println!("{}", rec1.get_longueur());
    println!("{}", rec1.surface());
    println!("{}", rec2.surface());

    // Cela passe avec un type générique car un type générique peut être n'importe quoi.
    // let rec3 = Rectangle{longueur:"15.5", largeur:"23.4"};
}