use std::io::{Write, BufRead, BufReader, stdin};
use std::fs::{OpenOptions, File};

pub fn input_output() {
    println!("Gestion des entrées et sorties");

    let path = "test_oi.txt";

    let mut s = String::new();
    // Récupération de ligne écrite dans le prompt et stockage dans la string s.
    stdin().read_line(&mut s).unwrap();

    // Création du fichier.
    let mut output = File::create(path).unwrap();
    // On écrit dans le fichier en convertisant la string en bytes.
    output.write(s.to_uppercase().as_bytes());

    // Lecture du contenu du fichier
    let input = File::open(path).unwrap();
    let buf = BufReader::new(input);

    for line in buf.lines() {
        println!("{}", line.unwrap());
    }

    input_output_open_options_method();
}

pub fn input_output_open_options_method() {
    println!("Gestion des entrées et sorties - méthode avec la lib OpenOptions");

    let mut file = OpenOptions::new()
        .write(true)
        .append(true)
        .create(true)
        .open("test_oi_open_option.txt")
        .unwrap()
    ;

    let mut s = String::new();
    stdin().read_line(&mut s).unwrap();
    file.write(s.as_bytes());
}