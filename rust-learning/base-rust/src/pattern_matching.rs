pub fn pattern_matching() {
    println!("Pattern Matching");

    let a = 32;

    // Equivalent au switch .. case
    // Les test se fait dans l'ordre des lignes.
    // Les break sont automatiques.
    match a {
        0 => println!("zéro"),
        // n if n < 0 => println!("erreur"), // Est-ce que a est égal à n tel que n < 0. On appelle ça un guard.
        _ if a < 0 => println!("erreur"), // Autre notation pour le garde.
        1..=17 => println!("mineur"), // Systéme de range : est-ce que a est compris en 1 et 17 inclus.
        19 | 24 | 32 => println!("Toto à la mer"),
        _ => println!("majeur"), // cas par défaut.
    }

    // Pattern binding
    // Notes l'utilisation de _ quand on veux ignorer l'assignation d'une valeur.
    let (a, (_, b)) = (56, (12, 124)); // La notation () s'appelle un tuple. Dedans les données n'ont pas besoin d'être homogénes.
    println!("{} - {}", a, b);

    // Match + Pattern binding
    // Exemple d'une calculette
    let calc = ("div", 50, 10);

    let (text, res) = match calc {
        (op, x, y) if op == "div" => ("Division", x/y),
        (op, x, y) if op == "add" => ("Addition", x+y),
        (op, x, _   ) if op == "square" => ("Carré", x*x),
        _ => ("Aucun calcul correspondant", 0),
    };

    println!("Opération {} - Résultat - {}", text, res);
}