// Ceci est une directive pour le compilateur.
// En outre, celui ci permet d'afficher les contenus demandés en debug.
#[derive(Debug)]

struct Personne {
    nom: String,
    age: u8,
}

impl Personne {
    // Méthode associé : méthode statiques qui n'a pas besoin d'une instance préalable.
    // Exemple avec un constructeur.
    fn new(n: String, a: u8) -> Personne {
        return Personne { nom: n, age: a };
    }

    // Méthode d'instance
    fn hello(&self) {
        println!("Salut ! Mon nom est {} et j'ai {} ans", self.nom, self.age);
    }
}

pub fn struct_rust() {
    println!("Struct (Objets)");
    let dude = Personne::new("Paul".to_owned(), 32);

    println!("{:?}", dude);
    println!("{} - {}", dude.nom, dude.age);
    dude.hello();
}