// Enum standard pour le traitement des erreurs.
// enum Result<T, E> {
//     Ok(T),
//     Err(E),
// }

use std::fs::File;

pub fn error_management() {
    println!("Gestion des erreurs");
    // Panic : cas classique pour forcer le programme à s'arrêter.
    // panic!("Crash du programme");

    // .unwrap permet de faire la gestion des erreurs commme le match en dessous.
    // let f = File::open("ressources/salut5.txt").unwrap();

    // .expect fait comme unwrap mais avec l'option d'ajout de message personnalisée.
    let f = File::open("ressources/salut.txt").expect("Lecture impossible");

    // Exemple avec arrêt du programme.
    // let file = match f {
    //     Ok(content) => content,
    //     Err(error) => {
    //         panic!("{}", error);
    //     },
    // };
}