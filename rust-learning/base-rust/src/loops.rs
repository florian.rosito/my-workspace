pub fn loops() {
    println!("Boucles");

    println!("While");
    let mut a = 0;
    while a < 10 {
        a += 1;
        if a % 2 == 0 {
            continue;
        }

        if a == 7 {
            break;
        }

        println!("{}", a);
    }

    println!("Loop");
    a = 0;
    // Equivaut à while true, permet de faire des boucles infinies.
    let result = loop {
        a += 1;
        if a == 10 {
            break a;
        }
    };
    println!("{}", result);

    println!("For");
    let b = [10,20,30,40,50];

    for elem in b.iter(){
        println!("{}", elem);
    }

    for (key, elem) in b.iter().enumerate() {
        println!("{}: {}", key, elem);
    }

    for elem in 1..10 {
        println!("{}", elem);
    }

    // Utilisation des labels pour les boucles
    'boucle1: for elem1 in 1..10 {
        for elem2 in 1..10 {
            if elem2 == 4 {
                break 'boucle1;
            }

            println!("{} - {}", elem1, elem2);
        }
    }
}