use std::collections::HashMap;

pub fn collection() {
    println!("Collections");

    println!("Tableau");
    // Array ont un taille fixe.
    let mut a: [u8;4] = [1,2,3,4];
    let b = [[1,2],[3,4]];
    a[1] *= 5;
    // {:?} est un opérateur de débug qui permet d'afficher des éléments compléxes.
    println!("{:?} - taille : {}", a, a.len());
    println!("{:?}", b);

    println!("Slice");
    let mut a = [45,12,78,65,32,45,87];
    // On crée un slice en faisant un pointeur vers une tranche du tableau pointé.
    let b = &mut a[0..4];
    b[0] = 12454;
    println!("{:?}", b);
    println!("{:?}", a);
    println!("{}", a[4]);

    println!("Tuples");
    // Comme le tableau, la taille est fixe.
    let t1 = (10,24.6,"string");
    let t2: (u8, i32, usize, f32) = (12,45,89,20.7);
    let t3 = (10,(24.6,"string"));
    println!("{}", t2.2);
    println!("{}", (t3.1).0);

    println!("Vectors");
    // Les vecteurs sont des listes de valeurs d'un même type.
    let mut vec1 = vec![12,45,78,63];
    vec1.push(125);
    vec1.remove(1);
    println!("{:?} - {} - {:?}", vec1, vec1[2], vec1.get(2));
    // L'intérêt de la méthode get() est qu'il retourne None si l'index demandé n'existe pas.
    // Au lieu de planté comme un accession via [].
    match vec1.get(53) {
        Some(x) => println!("{}", x),
        None => println!("Pas de valeur existante"),
    }
    // Comme c'est un tableau on peut faire des slices d'un vecteurs.
    let vec2 = vec!["string", "test", "toto", "blabal"];
    let slice1 = &vec2[0..2];
    println!("{:?}", slice1);

    // Méthode pour initier un vecteurs vide.
    let mut vec3 = Vec::new();
    vec3.push(54.5);
    // Cela ne marche pas car la premiére référence pousser détermine le type des valeurs du vecteur.
    // vec3.push("blabla");
    println!("{:?}", vec3);

    println!("Hashmap");
    let mut hash = HashMap::new();
    hash.insert("a", 1);
    hash.insert("b", 2);
    hash.insert("c", 3);
    println!("{:?}", hash);
    println!("{:?}", hash.len());
    println!("{:?}", hash.contains_key("b"));

    println!("Strings & &str");
    // Les strings sont aussi des collections de caractéres.
    let str = String::from("Salut à tous");
    let substr = &str[3..9];
    println!("{:?}", substr);
}